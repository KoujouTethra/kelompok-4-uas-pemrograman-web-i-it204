<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Profile Kami - UAS Pemrograman Web 1 | IT204 - Kelompok 4</title>
    <link rel="shortcut icon" href="assets/images/logo-unsia-only.png" type="image/x-icon">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Profile Kami - UAS Pemrograman Web 1 | IT204 - Kelompok 4" />
    <meta property="og:description" content="IT-204 | Kelompok 4 Yang terdiri dari Arwan, Rafsanjani Hafidh Firmansyah, Krisna Aji Putra, Iqbal Hadi Nugraha" />
    <meta property="og:image" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; ?>/assets/images/logo-unsia-only.png" />
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <meta property="og:image:type" content="image/png" />
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Work+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
</head>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<body>
<div id="preloader">
	<div id="status">
        <h4 class="mt-auto" style="margin-bottom: -40px;">Memuat...</h4>
    </div>
</div>
<div>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white shadow-sm">
        <div class="container py-2">
            <a class="navbar-brand" href="#">
                <img src="https://unsia.ac.id/wp-content/uploads/2022/11/LOGO-UNSIA-1-300x93.png" alt="Logo Unsia" height="42" class="d-inline-block align-text-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar2" aria-controls="offcanvasNavbar2" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end text-bg-light" tabindex="-1" id="offcanvasNavbar2" aria-labelledby="offcanvasNavbar2Label">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbar2Label">Universitas Siber Asia</h5>
                    <button type="button" class="btn-close btn-close-dark" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-start flex-grow-1 pe-3">
                        <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link active" href="profile-kami.php">Profile Kami</a>
                        </li>
                    </ul>
                    <div class="d-flex ml-auto align-items-center">
                        <b class="text-primary">IT-204 - Kelompok 4</b>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="container-fluid" style="padding-top: 140px;">
    <div class="container bg-white shadow pt-4 px-md-4 pb-5 text-center">
        <h1 class="fw-bold text-primary">Team Kami</h1>
        <h2 class="fw-bold">Kelompok 4</h2>
        <h3 class="text-primary">UAS Pemrograman Web 1</h3>
        <div class="row mt-4">
            <div class="col-md-3 p-3">
                <div class="card w-100 pt-4 shadow-sm border-0 h-100">
                    <img src="./assets/images/foto-arwan.png" class="card-img-top m-auto" style="width:240px; height:200px; object-fit:cover;" alt="Foto Arwan">
                    <div class="card-body">
                        <h5 class="card-title text-center">Arwan</h5>
                        <p class="card-text text-center ">NIM 220401010092</p>
                    </div>
                    <div class="card-footer mt-auto text-center">
                        <h6>Universitas Siber Asia<br>IT 204</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p-3">
                <div class="card w-100 pt-4 shadow-sm border-0 h-100">
                    <img src="./assets/images/foto-jani.png" class="card-img-top m-auto" style="width:240px; height:200px; object-fit:cover;" alt="Foto Rafsanjani Hafidh Firmansyah">
                    <div class="card-body">
                        <h5 class="card-title text-center">Rafsanjani Hafidh Firmansyah</h5>
                        <p class="card-text text-center ">NIM 220401010090</p>
                    </div>
                    <div class="card-footer mt-auto text-center">
                        <h6>Universitas Siber Asia<br>IT 204</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p-3">
                <div class="card w-100 pt-4 shadow-sm border-0 h-100">
                    <img src="./assets/images/foto-krisna.png" class="card-img-top m-auto" style="width:240px; height:200px; object-fit:cover;" alt="Foto Krisna Aji Putra">
                    <div class="card-body">
                        <h5 class="card-title text-center">Krisna Aji Putra</h5>
                        <p class="card-text text-center ">NIM 220401010196</p>
                    </div>
                    <div class="card-footer mt-auto text-center">
                        <h6>Universitas Siber Asia<br>IT 204</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-3 p-3">
                <div class="card w-100 pt-4 shadow-sm border-0 h-100">
                    <img src="./assets/images/foto-iqbal.png" class="card-img-top m-auto" style="width:240px; height:200px; object-fit:cover;" alt="Foto Iqbal Hadi Nugraha">
                    <div class="card-body">
                        <h5 class="card-title text-center">Iqbal Hadi Nugraha</h5>
                        <p class="card-text text-center ">NIM 220401070409</p>
                    </div>
                    <div class="card-footer mt-auto text-center">
                        <h6>Universitas Siber Asia<br>IT 204</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="mt-5">
    <div class="container-fluid shadow-lg" style="border-bottom: 34px solid #134a7b;">
        <div class="container bg-white d-flex justify-content-center p-5">
            <div class="col-md-3">
                <img src="assets/images/logo-unsia.png" alt="Logo Unsia Footer" class="w-100">
            </div>
        </div>
    </div>
</footer>

</body>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script>
		$(window).on('load', function() { 
			$('#status').delay(600).fadeOut(); 
            $('#preloader').fadeOut('slow'); 
            $('body').delay(600).css({'overflow':'visible'});
		})
    </script>
</html>