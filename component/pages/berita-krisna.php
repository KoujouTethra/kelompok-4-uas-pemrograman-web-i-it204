<style>
  /* Gaya untuk h1 */
  h1 {
    color: #333;
    font-size: 28px;
    font-weight: bold;
  }

  /* Gaya untuk p dengan class text-muted */
  .text-muted {
    color: #777;
    font-style: italic;
  }

  /* Gaya untuk card mb-3 */
  .card.mb-3 {
    margin-bottom: 20px;
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
    border: 1px solid #ccc;
    border-radius: 8px;
  }

  /* Gaya untuk card-body */
  .card-body {
    padding: 20px;
  }

  /* Gaya untuk card-title */
  .card-title {
    color: #007BFF;
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 10px;
  }

  /* Gaya untuk card-text */
  .card-text {
    color: #333;
    line-height: 1.6;
  }

  .vid-header {
    width: 100%;
    height: auto;
    position: relative;
    padding-inline: 25px;
    padding-top: 25px;
    padding-bottom: 25px;
  }

  .video-player {
    height: 350px;
    width: auto;
    position: relative;
    padding-inline: 25px;
  }
</style>
<header>
  <h1 class="h1-heading">Prajurit TNI Aktif Boleh Duduki Jabatan Sipil, tapi Saat Korupsi Ogah
    Tunduk Hukum Sipil</h1>
</header>
<div class="card mb-3">
  <img src="./assets/images/Artikel-Krisna.jpeg" class="img-fluid" alt="Gambar artikel krisna">
</div>
<div class="container">
  <p class="text-muted">Penulis Vitorio Mantalean | Editor Novianti Setuningsih JAKARTA,
    KOMPAS.com</p>
  <h5 class="card-title">JAKARTA, KOMPAS.com - Porsi prajurit TNI aktif menduduki jabatan
    sipil dianggap perlu dievaluasi buntut kisruh penanganan kasus suap yang menyeret
    nama Kepala Badan Nasional Pencarian dan Pertolongan (Basarnas) Marsekal Madya Henri
    Alfiandi.</h5>
  <p class="card-text">Sebelumnya, Henri Alfiandi sudah ditetapkan KPK sebagai tersangka
    dugaan suap pengadaan sejumlah proyek di Basarnas hingga Rp 88,3 miliar sejak
    2021-2023. Namun, polemik muncul setelahnya. Pusat Polisi Militer (Puspom) TNI
    merasa, Henri yang berstatus prajurit TNI aktif mestinya diproses hukum oleh mereka,
    bukan oleh KPK kendati kepala Basarnas adalah jabatan sipil. KPK akhirnya
    menyerahkan kasus yang diduga melibatkan Henri Alfiandi ke Puspom TNI.</p>
  <p class="card-text">"Ini menghidupkan kembali status anggota TNI sebagai warga negara
    kelas satu dan merupakan wujud inkonsistensi kebijakan," kata Direktur Eksekutif
    Amnesty International, Usman Hamid, secara daring dalam diskusi terbuka sejumlah
    elemen masyarakat sipil di kawasan Tebet, Jakarta Selatan, Minggu (30/7/2023).</p>
  <div class="d-flex justify-content-center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/2E6brzOijBk" title="YouTube video player"
      frameborder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      allowfullscreen></iframe>
  </div>
  <p class="card-text">"Ini menghidupkan kembali status anggota TNI sebagai warga negara
    kelas satu dan merupakan wujud inkonsistensi kebijakan," kata Direktur Eksekutif
    Amnesty International, Usman Hamid, secara daring dalam diskusi terbuka sejumlah
    elemen masyarakat sipil di kawasan Tebet, Jakarta Selatan, Minggu (30/7/2023).
    "Prajurit TNI aktif boleh duduk di jabatan sipil, tapi ketika korupsi tidak mau
    tunduk pada hukum sipil. Ini inkonsistensi kebijakan," ujarnya lagi.</p>
  <p class="card-text">"Prajurit TNI aktif boleh duduk di jabatan sipil, tapi ketika
    korupsi tidak mau tunduk pada hukum sipil. Ini inkonsistensi kebijakan," ujarnya
    lagi.</p>
  <p class="card-text">Undang-undang Nomor 34 Tahun 2004 tentang TNI sebetulnya mengatur
    bahwa jabatan sipil hanya dapat diduduki prajurit yang sudah pensiun atau mundur.
    Hal itu termaktub dalam Pasal 47 ayat (1). Namun, pada ayat (2), UU TNI mengatur ada
    sejumlah jabatan sipil yang diperbolehkan diisi prajurit aktif, yaitu kantor yang
    berkenaan dengan politik dan keamanan negara, pertahanan, sekretaris militer
    presiden, intelijen negara, sandi negara, lembaga ketahanan nasional, dewan
    pertahanan nasional, search and rescue (sar) nasional, narkotika nasional, dan
    Mahkamah Agung.</p>
  <p class="card-text">Namun, itu bukan berarti jabatan itu harus berasal dari unsur
    tentara. Di samping itu, Pasal 47 ayat (3) beleid yang sama menegaskan bahwa
    prajurit yang duduk di beberapa lembaga, termasuk Basarnas, harus tunduk pada
    ketentuan administrasi yang berlaku dalam lingkungan itu.</p>
  <div class="d-flex justify-content-center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/8Zwj-6E2QEA" title="YouTube video player"
      frameborder="0"
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
      allowfullscreen></iframe>
  </div>

  <p class="card-text">Usman Hamid menegaskan bahwa Basarnas adalah lembaga dengan jabatan
    sipil. Oleh karena itu, kasus hukum yang menjerat pejabat Basarnas semestinya tunduk
    pada peradilan sipil. Apalagi, Pasal 42 Undang-undang Nomor 30 Tahun 2002 tentang
    KPK menegaskan bahwa lembaga antirasuah itu "berwenang mengkoordinasikan dan
    mengendalikan penyelidikan, penyidikan, dan penuntutan tindak pidana korupsi yang
    dilakukan bersama-sama oleh orang yang tunduk pada peradilan militer dan peradilan
    umum".</p>
  <p class="card-text">Pasal 65 ayat (2) UU TNI juga menegaskan bahwa prajurit hanya
    tunduk kepada kekuasaan peradilan militer "dalam hal pelanggaran hukum pidana
    militer".</p>
  <p class="card-text">Sejauh ini, anggapan bahwa Henri Alfiandi harus diproses secara
    militer berangkat dari Pasal Undang-undang Nomor 31 Tahun 1997 tentang Peradilan
    Militer. Usman Hamid menilai, beleid ini seharusnya sudah dikesampingkan oleh
    berbagai undang-undang yang lebih baru di atas.</p>
  <p class="card-text">Direktur Lingkar Madani Indonesia, Ray Rangkuti, juga berpendapat
    senada. Ia menilai, kasus ini harus dijadikan evaluasi keterlibatan TNI di ranah
    sipil. "Kalau militer tetap menganggap dirinya militer di mana pun berada, ya malau
    begitu kita harus persempit ruangnya," ujar Ray dalam kesempatan yang sama.</p>
</div>