<div style="width:100%; height:100%;">
    <h1 style="font-size:30px">Pencarian 8 Penambang Terjebak di Banyumas Disetop, Korban Dinyatakan Hilang</h1>
    <small><i>Sabtu, 29 Juli 2023</i></small>
    <hr>
	<img src="https://akcdn.detik.net.id/community/media/visual/2023/08/01/upacara-penutupan-operasi-pencarian-8-penambang-yang-terjebak-air-di-lubang-galian-emas-desa-pancurendang-kecamatan-ajibarang-_43.jpeg?w=700&q=90" style="width:100%" class="p_img_zoomin img-zoomin" />
	<figcaption class="detail__media-caption">Upacara penutupan operasi pencarian 8 penambang yang terjebak air di lubang galian emas Desa Pancurendang, Kecamatan Ajibarang, Banyumas, Selasa (1/8/2023). Foto: Anang Firmansyah/detikJateng</figcaption>
    </figure>
	<hr>
	<div>
		<strong>Banyumas</strong> - <p>Proses pencarian terhadap delapan penambang yang terjebak air di lubang galian emas Desa Pancurendang, Kecamatan Ajibarang, Banyumas dihentikan. Keputusan tersebut diambil berdasarkan rapat koordinasi dengan Forkompinda Banyumas.</p>
		<p>Kepala Basarnas Cilacap, Adah Sudarsa mengatakan sesuai SOP Basarnas, apabila tanda-tanda korban tidak ditemukan setelah pencarian tujuh hari maka operasi pencarian bisa dinyatakan ditutup.</p>
		<p>"Apabila tanda-tanda korban tidak ditemukan ataupun tidak efisiensi lagi dalam pelaksanaannya, operasi SAR itu bisa dinyatakan ditutup," kata Adah kepada wartawan di lokasi, Selasa (1/8/2023).</p>
		<p>Adah melanjutkan, apabila suatu hari nanti ada hal-hal yang di luar perkiraan, operasi pencarian bisa dibuka kembali.</p>
		<p>"Kalau memang ada tanda-tanda itu ya kita bisa laksanakan operasi SAR kembali," ujarnya.</p>
		<p>Adah menjelaskan, saat ini delapan penambang tersebut statusnya dinyatakan hilang. "Para korban kita nyatakan hilang," ungkapnya.</p><p>Ia juga mengucapkan belasungkawa terhadap keluarga korban yang hadir dalam agenda penutupan operasi pencarian.</p>
		<p>"Kami turut prihatin. Semoga korban yang ditinggalkan diberikan ketabahan dan kesabaran dalam menghadapi masalah ini," ujarnya.</p>
		<p>Sementara itu Danrem 071 Wijayakusuma, Kolonel Czi Andhy Kusuma menjelaskan kendala utama dalam operasi pencarian kali ini adalah lubang tertutup genangan air.</p>
		<p>"Karena lubang yang ada tergenang air sehingga menyulitkan kita untuk evakuasi. Kemudian kondisi lubang yang dalam dan sangat sempit," katanya.</p>
		<p>Pihaknya sudah mendatangkan berbagai macam alat untuk menyedot air. Baik yang ada di atas permukaan maupun dalam permukaan.</p>
		<p>"Namun karena kita melawan alam, debitnya lebih tinggi, sehingga sampai dengan saat ini belum ada perkembangan yang signifikan. Kita juga sudah melakukan upaya pembendungan sumber air, tapi masih belum memberikan hasil maksimal," pungkasnya.</p>
		<p>Diberitakan sebelumnya, delapan penambang emas dilaporkan terjebak di lubang galian tambang emas di Desa Pancurendang, Kecamatan Ajibarang, Banyumas.</p>
		<p>Kapolresta Banyumas Kombes Edy Suranta Sitepu menjelaskan kronologi kejadian tersebut bermula saat adanya aktivitas tambang yang dilakukan delapan warga pada Selasa (25/7) malam.</p>
		<p>Edy menyampaikan para penambang sudah mulai bekerja sejak Selasa (25/7) malam pukul 20.00 WIB. Dua jam setelah melakukan penggalian, ada informasi jika air sudah mulai mengalir dari lokasi yang ada di sebelahnya.</p>
		<p>"Informasi yang kami dapatkan tadi mereka mulai bekerja dari jam 20.00 WIB kemudian jam 22.00 WIB sudah ada informasi bahwa sudah ada air yang mulai mengalir dari lokasi sebelah," kata Edy kepada wartawan, Rabu (26/7).</p>
		
		<iframe class="video-yutup-iqbal mb-3" src="https://20.detik.com/embed/230726141" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
        <i class="w-100">Sumber : <a href="https://www.detik.com/jateng/berita/d-6853094/pencarian-8-penambang-terjebak-di-banyumas-disetop-korban-dinyatakan-hilang" target="_blank" rel="noopener noreferrer">Detik - Pencarian 8 Penambang Terjebak di Banyumas Disetop, Korban Dinyatakan Hilang</a></i>
        <p class="mt-3">Reupload by : <b>Iqbal Hadi Nugraha</b></p>
    </div>
	
</div>

