<?php
    $page = $_GET['page'];

    $pages = array(
        "5 Efek Badai Matahari terhadap Bumi, Termasuk 'Kiamat' Internet" => array(
            'file' => './component/pages/berita-arwan.php',
            'photo' => './assets/images/foto-arwan.png',
            'nama' => 'Arwan'
        ),
        'Harga Emas Terbaru Antam 29 Juli 2023, Termurah Rp 585.500 dan Termahal Rp 1.011.600.000' => array(
            'file' => './component/pages/berita-jani.php',
            'photo' => './assets/images/foto-jani.png',
            'nama' => 'Rafsanjani Hafidh Firmansyah'
        ),
        'Prajurit TNI Aktif Boleh Duduki Jabatan Sipil, tapi Saat Korupsi Ogah Tunduk Hukum Sipil' => array(
            'file' => './component/pages/berita-krisna.php',
            'photo' => './assets/images/foto-krisna.png',
            'nama' => 'Krisna Aji Putra'
        ),
        'Pencarian 8 Penambang Terjebak di Banyumas Disetop, Korban Dinyatakan Hilang' => array(
            'file' => './component/pages/berita-iqbal.php',
            'photo' => './assets/images/foto-iqbal.png',
            'nama' => 'Iqbal Hadi Nugraha'
        )
    );
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page ?> - UAS Pemrograman Web 1 | IT204 - Kelompok 4</title>
    <link rel="shortcut icon" href="assets/images/logo-unsia-only.png" type="image/x-icon">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $page ?> - UAS Pemrograman Web 1 | IT204 - Kelompok 4" />
    <meta property="og:description" content="IT-204 | Kelompok 4 Yang terdiri dari Arwan, Rafsanjani Hafidh Firmansyah, Krisna Aji Putra, Iqbal Hadi Nugraha" />
    <meta property="og:image" content="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]"; ?>/assets/images/logo-unsia-only.png" />
    <meta property="og:image:width" content="150" />
    <meta property="og:image:height" content="150" />
    <meta property="og:image:type" content="image/png" />
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&family=Work+Sans:ital,wght@0,400;0,500;0,600;0,700;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
</head>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<body>
<div id="preloader">
	<div id="status">
        <h4 class="mt-auto" style="margin-bottom: -40px;">Memuat...</h4>
    </div>
</div>
<div>
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white shadow-sm">
        <div class="container py-2">
            <a class="navbar-brand" href="#">
                <img src="https://unsia.ac.id/wp-content/uploads/2022/11/LOGO-UNSIA-1-300x93.png" alt="Logo Unsia" height="42" class="d-inline-block align-text-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar2" aria-controls="offcanvasNavbar2" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="offcanvas offcanvas-end text-bg-light" tabindex="-1" id="offcanvasNavbar2" aria-labelledby="offcanvasNavbar2Label">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasNavbar2Label">Universitas Siber Asia</h5>
                    <button type="button" class="btn-close btn-close-dark" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">
                    <ul class="navbar-nav justify-content-start flex-grow-1 pe-3">
                        <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="profile-kami.php">Profile Kami</a>
                        </li>
                    </ul>
                    <div class="d-flex ml-auto align-items-center">
                        <b class="text-primary">IT-204 - Kelompok 4</b>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
<div class="container-fluid" style="padding-top: 140px;">
    <div class="container bg-white shadow pt-md-3 px-md-4 pb-5">
        <div class="row">
            <div class="col-md-8 mb-3 pt-3">
                <?php
                    if (isset($pages[$page])) {
                        $content = include $pages[$page]['file'];
                        $photo = $pages[$page]['photo'];
                        $nama = $pages[$page]['nama'];
                    } else {
                        $photo = './assets/images/logo-unsia-only.png';
                        $nama = 'Universitas Siber Asia';
                        echo "<div class='d-flex justify-content-center align-items-center w-100 h-100'><div class='d-block justify-content-center align-items-center text-center'><img src='./assets/images/404.gif' class='w-75'><br><h4>Berita tidak ditemukan!</h4> </div></div>";
                    }
                    ?>
                <hr>
            </div>
            
            <div class="col-md-4">
                <div class="card w-100 pt-4">
                <img src="<?php echo $photo; ?>" class="card-img-top m-auto" style="border-radius:50%; width:200px; height:200px; object-fit:cover;" alt="Foto <?php echo $nama; ?>">
                <div class="card-body">
                    <p class="card-text text-center ">Dibuat Oleh</p>
                    <h5 class="card-title text-center"><?php echo $nama; ?></h5>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item bg-primary text-light">
                        <p class="mb-1 fw-bold h5 text-center">Lihat Berita Lainnya!</p>
                    </li>
                    <li class="list-group-item p-4 py-2 mt-2" style="border-left: 2px solid #134a7b">
                    <a href="lihat-berita.php?page=5 Efek Badai Matahari terhadap Bumi, Termasuk 'Kiamat' Internet" class="text-dark text-decoration-none">
                        <p class="mb-1 fw-bold berita-truncate-1">5 Efek Badai Matahari terhadap Bumi, Termasuk 'Kiamat' Internet</p>
                        <p class="mb-1 berita-truncate-1 text-primary"><small>Dibuat Oleh Arwan</small></p>
                    </a>
                    </li>
                    <li class="list-group-item p-4 py-2 mt-2" style="border-left: 2px solid #134a7b">
                    <a href="lihat-berita.php?page=Harga Emas Terbaru Antam 29 Juli 2023, Termurah Rp 585.500 dan Termahal Rp 1.011.600.000" class="text-dark text-decoration-none">
                        <p class="mb-1 fw-bold berita-truncate-1">Harga Emas Terbaru Antam 29 Juli 2023, Termurah Rp 585.500 dan Termahal Rp 1.011.600.000</p>
                        <p class="mb-1 berita-truncate-1 text-primary"><small>Dibuat Oleh Rafsanjani Hafidh Firmansyah</small></p>
                    </a>
                    </li>
                    <li class="list-group-item p-4 py-2 mt-2" style="border-left: 2px solid #134a7b">
                    <a href="lihat-berita.php?page=Prajurit TNI Aktif Boleh Duduki Jabatan Sipil, tapi Saat Korupsi Ogah Tunduk Hukum Sipil" class="text-dark text-decoration-none">
                        <p class="mb-1 fw-bold berita-truncate-1">Prajurit TNI Aktif Boleh Duduki Jabatan Sipil, tapi Saat Korupsi Ogah Tunduk Hukum Sipil</p>
                        <p class="mb-1 berita-truncate-1 text-primary"><small>Dibuat Oleh Krisna Aji Putra</small></p>
                    </a>
                    </li>
                    <li class="list-group-item p-4 py-2 mt-2" style="border-left: 2px solid #134a7b">
                    <a href="lihat-berita.php?page=Pencarian 8 Penambang Terjebak di Banyumas Disetop, Korban Dinyatakan Hilang" class="text-dark text-decoration-none">
                        <p class="mb-1 fw-bold berita-truncate-1">Pencarian 8 Penambang Terjebak di Banyumas Disetop, Korban Dinyatakan Hilang</p>
                        <p class="mb-1 berita-truncate-1 text-primary"><small>Dibuat Oleh Iqbal Hadi Putra</small></p>
                    </a>
                    </li>
                </ul>
            </div>
            </div>
        </div>
    </div>
</div>

<footer class="mt-5 bg-white">
    <div class="container-fluid shadow-lg" style="border-bottom: 34px solid #134a7b;">
        <div class="container bg-white d-flex justify-content-center p-5">
            <div class="col-md-3">
                <img src="assets/images/logo-unsia.png" alt="Logo Unsia Footer" class="w-100">
            </div>
        </div>
    </div>
</footer>

</body>
    <script src="assets/js/bootstrap.bundle.js"></script>
    <script>
		$(window).on('load', function() { 
			$('#status').delay(600).fadeOut(); 
            $('#preloader').fadeOut('slow'); 
            $('body').delay(600).css({'overflow':'visible'});
		})
    </script>
</html>